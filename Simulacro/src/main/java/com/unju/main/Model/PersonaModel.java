package com.unju.main.Model;

import java.util.Date;

public class PersonaModel {
	//Atributos
	private int dni;
	private String apellido;
	private String nombre;
	private Date fechaNac;
	
	
	
	//Constructores
	public PersonaModel(int dni, String apellido, String nombre, Date fechaNac) {
		this.dni = dni;
		this.apellido = apellido;
		this.nombre = nombre;
		this.fechaNac = fechaNac;
	}

	public PersonaModel() {
	}

	//Getters and Setters
	public int getDni() {
		return dni;
	}

	public void setDni(int dni) {
		this.dni = dni;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Date getFechaNac() {
		return fechaNac;
	}

	public void setFechaNac(Date fechaNac) {
		this.fechaNac = fechaNac;
	}
	
	
	
	
}
