package com.unju.main.Controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.unju.main.Model.PersonaModel;

@Controller
public class PersonaController {

	 List<PersonaModel> listaPersonas = new ArrayList<PersonaModel>();
	 private PersonaModel pers;
	 
	 @GetMapping("/inicio")
	 public String inicio(Model modelo) {	 	 
	 	 modelo.addAttribute("pers",listaPersonas);	 	 
	 	 return "formulario";
	  }
}
