package com.unju.main;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class SimulacroApplication {

	@GetMapping("/prueba")
	public String prueba() {
		return "Agregando funciones";
	}
	public static void main(String[] args) {
		SpringApplication.run(SimulacroApplication.class, args);
	}

}
